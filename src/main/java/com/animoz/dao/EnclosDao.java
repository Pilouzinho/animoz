package com.animoz.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.animoz.modele.Enclos;

@Repository
public class EnclosDao {

	@PersistenceContext
	private EntityManager em;
	
	public List<Enclos> getAllEnclos() {
		return em.createQuery("select e from Enclos e order by e.id", Enclos.class).getResultList();
	}
	
	public Enclos getEnclos(long enclosId) {
		return em.find(Enclos.class, enclosId);
	}
}
