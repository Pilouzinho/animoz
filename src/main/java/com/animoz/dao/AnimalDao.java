package com.animoz.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.animoz.modele.Animal;

@Repository
public class AnimalDao {
	
	@PersistenceContext
	private EntityManager em;

	public Animal getAnimal(long animalId) {
		return em.find(Animal.class, animalId);
	}

	public List<Animal> getAnimaux() {
		return em.createQuery("select a from Animal a order by a.nom", Animal.class).getResultList();
	}

	public boolean existe(String nomAnimal) {
		long nb = em.createQuery("select count(a) from Animal a where lower(a.nom) = lower(:nom)", Long.class)
			    .setParameter("nom", nomAnimal)
			    .getSingleResult();
	return nb > 0;
	}
	
	public boolean existeById(long idAnimal) {
		long nb = em.createQuery("select count(a) from Animal a where a.id = :id", Long.class)
					.setParameter("id", idAnimal)
					.getSingleResult();
		return nb > 0;
	}

	public long ajouter(Animal animal) {

		if(animal.getId() == null || !existeById(animal.getId()) ) {
			em.persist(animal);
			em.flush();
			return animal.getId();
		}
		
		em.merge(animal);
		em.flush();
		return animal.getId();
	}
	
	public int deleteAnimal(long idAnimal) {
		return em.createQuery("delete from Animal a where a.id = :id")
		  .setParameter("id", idAnimal)
		  .executeUpdate();
	}

}
