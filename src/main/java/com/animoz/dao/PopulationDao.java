package com.animoz.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import com.animoz.modele.Population;

@Repository
public class PopulationDao {

	@PersistenceContext
	private EntityManager em;
	
	public List<Population> getPopulations() {
		return em.createQuery("select p from Population p order by p.animal.nom", Population.class).getResultList();
	}
	
	public Population getPopulation(long populationId) {
		return em.find(Population.class, populationId);
	}
	
	public boolean existe(long  populationId) {
		long nb = em.createQuery("select count(p) from Population p where p.id = :id", Long.class)
			    .setParameter("id", populationId)
			    .getSingleResult();
		return nb > 0;
	}
	
	public long ajouter(Population population) {

		if(population.getId() == null || !existe(population.getId()) ) {
			em.persist(population);
			em.flush();
			return population.getId();
		}
		
		em.merge(population);
		em.flush();
		return population.getId();
	}
	
	public int deletePopulation(long populationId) {
		return em.createQuery("delete from Population p where p.id = :id")
		  .setParameter("id", populationId)
		  .executeUpdate();
	}
}
