package com.animoz.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.dao.PopulationDao;
import com.animoz.modele.Enclos;
import com.animoz.modele.Population;
import com.animoz.modele.Regime;

@Service
public class PopulationService {

	@Autowired
	private PopulationDao populationDao;
	
	@Autowired
	private EnclosService enclosService;
	
	public List<Population> getPopulations() {
		return populationDao.getPopulations();
	}

	public Population getPopulation(long populationId) {
		return populationDao.getPopulation(populationId);
	}
	
	@Transactional
	public long ajouter(Population population) {
		//rapatrier la vérif du dao ici
		if(isEnclosDangerous(population)) {
			return 0;
		}else {
			return populationDao.ajouter(population);
		}
	}
	
	@Transactional
	public boolean supprimer(long populationId) {
		if(populationDao.existe(populationId)) {
			int resultDel = populationDao.deletePopulation(populationId);
			if(resultDel > 0) {
				return true;
			}
		}
		return false;
	}
	
	public boolean isEnclosDangerous(Population population) {
		Regime regimeAnimalARajouter = population.getAnimal().getRegime();
		
		Enclos enclosAccueilNouvelAnimal = enclosService.getEnclos(population.getEnclos().getId());
		List<Population> populationsAChecker = enclosAccueilNouvelAnimal.getPopulations();
		for(Population p : populationsAChecker) {
			if (regimeAnimalARajouter == Regime.herbivore) {
				if (p.getAnimal().getRegime() == Regime.carnivore) {
					return true;
				}
			}else if(regimeAnimalARajouter == Regime.carnivore) {
				if (p.getAnimal().getRegime() == Regime.herbivore) {
					return true;
				}
			}
		}
		
		return false;
	}
}
