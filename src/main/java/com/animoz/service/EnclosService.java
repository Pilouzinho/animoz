package com.animoz.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.dao.EnclosDao;
import com.animoz.modele.Enclos;

@Service
public class EnclosService {
	
	@Autowired
	private EnclosDao enclosDao;

	public List<Enclos> getAllEnclos() {
		return enclosDao.getAllEnclos();
	}

	public Enclos getEnclos(long enclosId) {
		return enclosDao.getEnclos(enclosId);
	}
}
