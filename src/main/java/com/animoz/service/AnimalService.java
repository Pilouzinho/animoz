package com.animoz.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.animoz.dao.AnimalDao;
import com.animoz.modele.Animal;
import com.animoz.modele.Espece;

@Service
public class AnimalService {
	
	@Autowired
	private AnimalDao animalDao;

	public List<Animal> getAnimaux() {
		return animalDao.getAnimaux();
	}

	public Animal getAnimal(long animalId) {
		return animalDao.getAnimal(animalId);
	}
	
	@Transactional
//	public long ajouter(Animal animal) {
//		if(! animalDao.existe(animal.getNom())) {
//			return animalDao.ajouter(animal);
//			
//		}
//		return 0;
//	}
	
	public long ajouter(Animal animal) {
		//rapatrier la vérif du dao ici
		return animalDao.ajouter(animal);
	}
	
	@Transactional
	public boolean supprimer(long idAnimal) {
		if(animalDao.existeById(idAnimal)) {
			int resultDel = animalDao.deleteAnimal(idAnimal);
			if(resultDel > 0) {
				return true;
			}
		}
		return false;
	}

}
