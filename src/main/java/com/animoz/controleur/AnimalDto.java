package com.animoz.controleur;

import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotBlank;

import com.animoz.modele.Espece;
import com.animoz.modele.Regime;

public class AnimalDto {
	
	private long id;

	@NotBlank(message = "Vous devez fournir un nom pour l''animal !")
	@Size(min = 3, max = 100, message="Le nom doit être composé d''au moins 3 caractères et maximum 100")
	private String nom;
	
	@NotBlank(message = "Vous devez fournir une origine pour l''animal !")
	@Size(min = 3, max = 100, message="L''origine doit être composé d''au moins 3 caractères et maximum 100")
	private String origine;
	
	@NotBlank(message = "Vous devez fournir un descriptif (même bref) pour l''animal !")
	private String description;
	
	private Regime regime;
	
	private long especeId;
	
	
	
	public AnimalDto() {
		super();
	}



	public AnimalDto(long id, String nom, String origine, String description, Regime regime, long especeId) {
		super();
		this.id = id;
		this.nom = nom;
		this.origine = origine;
		this.description = description;
		this.regime = regime;
		this.especeId = especeId;
	}
	
	public long getEspeceId() {
		return especeId;
	}
	public void setEspeceId(long especeId) {
		this.especeId = especeId;
	}
	
	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	
	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getOrigine() {
		return origine;
	}
	public void setOrigine(String origine) {
		this.origine = origine;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Regime getRegime() {
		return regime;
	}
	public void setRegime(Regime regime) {
		this.regime = regime;
	}
	
	
}
