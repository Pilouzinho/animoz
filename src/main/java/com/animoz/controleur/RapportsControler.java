package com.animoz.controleur;

import java.io.InputStream;
import java.io.OutputStream;
import java.sql.Connection;
import java.util.HashMap;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;

@Controller
public class RapportsControler {
	
	@Autowired
	  private DataSource dataSource;
	
	private JasperReport rapport;
	private JasperReport rapportExcel;

	
	  @PostConstruct
	  public void compilerRapport() throws Exception {
	    InputStream modeleInputStream = this.getClass().getResourceAsStream("/rapportanimal.jrxml");
	    rapport = JasperCompileManager.compileReport(modeleInputStream);
	  }
	
	  @GetMapping(path="/rapport-animaux.pdf", produces = "application/pdf")
	  public void produireRapport(OutputStream out) throws Exception {

	    try(Connection connection = dataSource.getConnection()) {
	      Map<String, Object> parameters = new HashMap<>();
	      JasperPrint print = JasperFillManager.fillReport(rapport, parameters, connection);

	      JRPdfExporter pdfExporter = new JRPdfExporter();
	      pdfExporter.setExporterInput(new SimpleExporterInput(print));
	      pdfExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
	      pdfExporter.exportReport();
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	  }
	  
	  @PostConstruct
	  public void compilerRapportExcel() throws Exception {
	    InputStream modeleInputStream = this.getClass().getResourceAsStream("/populations.jrxml");
	    rapportExcel = JasperCompileManager.compileReport(modeleInputStream);
	  }
	  
	  @GetMapping(path="/rapport.xlsx")
	  public void produireRapportExcel(OutputStream out, HttpServletResponse response) throws Exception {

		 
	    try(Connection connection = dataSource.getConnection()) {
	      Map<String, Object> parameters = new HashMap<>();
	      JasperPrint print = JasperFillManager.fillReport(rapportExcel, parameters, connection);

	      
          
	      JRXlsxExporter xlsxExporter = new JRXlsxExporter();
	      xlsxExporter.setExporterInput(new SimpleExporterInput(print));
	      xlsxExporter.setExporterOutput(new SimpleOutputStreamExporterOutput(out));
	      xlsxExporter.exportReport();
          //response.setContentType("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
	    }catch(Exception e) {
	    	e.printStackTrace();
	    }
	  }
}
