package com.animoz.controleur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import org.springframework.web.servlet.view.RedirectView;

import com.animoz.modele.Animal;
import com.animoz.modele.Espece;
import com.animoz.modele.Regime;
import com.animoz.service.AnimalService;
import com.animoz.service.EspeceService;

@Controller
public class AnimalControleur {
	
	@Autowired
	private AnimalService animalService;
	
	@Autowired
	private EspeceService especeService;
	
	@GetMapping("/animal")
	public String getListeAnimaux(Model model) {
		model.addAttribute("animaux", animalService.getAnimaux());
		return "animaux";
	}
	
	@GetMapping("/animal-ajout/{idAnimalAjout}")
	public String getFormAnimaux(Model model, @ModelAttribute AnimalDto animalDto, @PathVariable long idAnimalAjout) {
		if(idAnimalAjout > 0) {
			Animal animalAModifier = animalService.getAnimal(idAnimalAjout);
			AnimalDto animalDtoModel = animalDto;
				animalDtoModel = new AnimalDto(animalAModifier.getId(), animalAModifier.getNom(), animalAModifier.getOrigine(), animalAModifier.getDescription(), animalAModifier.getRegime(), animalAModifier.getEspece().getId());
			model.addAttribute("animalDto", animalDtoModel);
		}
		model.addAttribute("especes", especeService.getEspeces());
		model.addAttribute("regimes", Regime.values());
		return "animauxAjout";
	}

	@GetMapping("/animal/{animalId}")
	public String getAnimal(Model model, @PathVariable long animalId) {
		model.addAttribute("animal", animalService.getAnimal(animalId));
		return "animal";
	}
	
	@PostMapping("/animal-ajout")
	public String ajouterAnimal(Model model,  @Valid @ModelAttribute AnimalDto animalDto, BindingResult binding) {

		if(binding.hasErrors()) {
			System.out.println(animalDto.getEspeceId());
			for(ObjectError e : binding.getAllErrors()) {
				System.out.println(e.getDefaultMessage());
			}
			return getAnimal(model, animalDto.getId());
		}
		
		Espece espece = especeService.getEspece(animalDto.getEspeceId());
		Animal animalAjout = new Animal(animalDto.getNom(), animalDto.getOrigine(), animalDto.getDescription(), animalDto.getRegime(), espece, null);
		if(animalDto.getId() > 0) {
			animalAjout.setId(animalDto.getId());
		}
		long newAnimal = animalService.ajouter(animalAjout);
		return "redirect:/animal/"+newAnimal;
		
	}
	
	@GetMapping("/animal-supprimer/{idAnimal}")
	public String supprimerAnimal(Model model, @PathVariable long idAnimal, RedirectAttributes redirectAttributes) {
		boolean suppression = animalService.supprimer(idAnimal);
		if(suppression) {
			return getListeAnimaux(model);
		}
		redirectAttributes.addFlashAttribute("messageErreur", "La suppression n'a pas pu être effectuée");
		return getListeAnimaux(model);
	}
}
