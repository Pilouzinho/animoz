package com.animoz.controleur;

import javax.validation.constraints.Min;

public class PopulationDto {

	private long id;
	
	//annotation ne fonctionne pas sans validator ? fait la vérification dans le setter
	@Min(value=1, message="Le nombre doit être au moins égal à 1")
	private int nombreIndividus = 1;
	
	private long idAnimal;
	private long idEnclos;
	
	public PopulationDto() {
		super();
	}

	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getNombreIndividus() {
		return nombreIndividus;
	}

	public void setNombreIndividus(int nombreIndividus) {
		if(nombreIndividus < 0) {
			this.nombreIndividus = nombreIndividus * -1;
		}else {
			this.nombreIndividus = nombreIndividus;
		}
	}



	public long getIdAnimal() {
		return idAnimal;
	}



	public void setIdAnimal(long idAnimal) {
		this.idAnimal = idAnimal;
	}



	public long getIdEnclos() {
		return idEnclos;
	}



	public void setIdEnclos(long idEnclos) {
		this.idEnclos = idEnclos;
	}



	@Override
	public String toString() {
		return "PopulationDto [id=" + id + ", nombreIndividus=" + nombreIndividus + ", idAnimal=" + idAnimal
				+ ", idEnclos=" + idEnclos + "]";
	}

	
	
	
	
	
	
}
