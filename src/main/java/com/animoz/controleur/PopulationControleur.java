package com.animoz.controleur;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.animoz.modele.Animal;
import com.animoz.modele.Enclos;
import com.animoz.modele.Population;
import com.animoz.service.AnimalService;
import com.animoz.service.EnclosService;
import com.animoz.service.PopulationService;

@Controller
public class PopulationControleur {

	@Autowired
	private AnimalService animalService;
	
	@Autowired
	private EnclosService enclosService;
	
	@Autowired
	private PopulationService populationService;
	
	@GetMapping("/populations")
	public String getListePopulation(Model model) {
		model.addAttribute("populations", populationService.getPopulations());
		return "populations";
	}
	
	@GetMapping("/population-ajout/{idPopulationAjout}")
	public String getFormPopulation(Model model, @ModelAttribute PopulationDto populationDto, @PathVariable long idPopulationAjout) {
		if(idPopulationAjout > 0) {
			Population populationAModifier = populationService.getPopulation(idPopulationAjout);
			
			populationDto.setId(populationAModifier.getId());
			populationDto.setNombreIndividus(populationAModifier.getNombreIndividus());
			populationDto.setIdAnimal(populationAModifier.getAnimal().getId());
			if(populationAModifier.getEnclos() != null) {populationDto.setIdEnclos(populationAModifier.getEnclos().getId());};
			model.addAttribute("populationDto", populationDto);
		}
		model.addAttribute("animaux", animalService.getAnimaux());
		model.addAttribute("encloss", enclosService.getAllEnclos());
		return "populationAjout";
	}
	
	@PostMapping("/population-ajout")
	public String postFormPopulation(Model model,  @Valid PopulationDto populationDto, BindingResult binding, RedirectAttributes redirectAttributes) {
		if(binding.hasErrors()) {
			System.out.println(populationDto.toString());
			for(ObjectError e : binding.getAllErrors()) {
				System.out.println(e.getDefaultMessage());
			}
			
		}
		System.out.println(populationDto.toString());
		
		Animal animalPop = animalService.getAnimal(populationDto.getIdAnimal());
		Enclos enclosPop = enclosService.getEnclos(populationDto.getIdEnclos());
		Population popAjout = new Population(populationDto.getNombreIndividus(), animalPop, enclosPop);
		
		if(populationDto.getId() > 0) {
			popAjout.setId(populationDto.getId());
		}
		long ajoutOk = populationService.ajouter(popAjout);
		if(ajoutOk > 0) {
			redirectAttributes.addFlashAttribute("messageAjoutModif", "L'ajout/modification a bien été effectuée");
		}else {
			redirectAttributes.addFlashAttribute("messageAjoutModif", "Le régime alimentaire des animaux déjà dans l'enclos est incompatible avec les animaux que vous souhaitez rajouter");
		}
		return "redirect:/populations";
	}
	
	@GetMapping("/population-supprimer/{idPopulation}")
	public String supprimerPopulation(Model model, @PathVariable long idPopulation, RedirectAttributes redirectAttributes) {
		boolean suppression = populationService.supprimer(idPopulation);
		if(suppression) {
			return getListePopulation(model);
		}
		redirectAttributes.addFlashAttribute("messageErreur", "La suppression n'a pas pu être effectuée");
		return getListePopulation(model);
	}
}
