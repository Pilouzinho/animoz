package com.animoz.controleur;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.animoz.service.AnimalService;
import com.animoz.service.EnclosService;
import com.animoz.service.PopulationService;

@Controller
public class EnclosControleur {

	@Autowired
	private AnimalService animalService;
	
	@Autowired
	private EnclosService enclosService;
	
	@Autowired
	private PopulationService populationService;
	
	@GetMapping("/enclos")
	public String getListePopulation(Model model) {
		model.addAttribute("enclos", enclosService.getAllEnclos());
		return "enclos";
	}
}
