<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>

	<ul>
		<c:forEach items="${especes}" var="espece">
			<li><c:out value="${espece.nom}"/></li>
		</c:forEach>
		<li>
			<form:form servletRelativeAction="/espece" modelAttribute="especeDto">
				<form:input path="nom"/> <button type="submit">Ajouter</button>
				<form:errors path="nom"/>
			</form:form>
		</li>
	</ul>
	<div><c:out value="${messageSuppression}"/></div>
	<form:form servletRelativeAction="/espece/disparition" method="post" modelAttribute="especeDto">
		<form:select path="nom">
			<form:options items="${especes}" itemLabel="nom" itemValue="nom"/>
		</form:select>
		<button type="submit">Supprimer</button>
	</form:form>

<jsp:include page="footer.jsp"></jsp:include>