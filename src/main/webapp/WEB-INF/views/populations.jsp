<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>

<h1>Liste des populations</h1>
<p><c:out value="${messageAjoutModif}" /></p>
<ul>
		<c:forEach items="${populations}" var="population">
			<li style="margin: 40px 0;">
				<h3>Population : <c:out value="${population.animal.nom}"/></h3>
				<p>Nombre d'individus dans cette population  :  <c:out value="${population.nombreIndividus}"/></p>
				<p style="margin: 15px 0;">Numéro de l'encolos où se trouve la population : <c:out value="${(not empty population.enclos.numero) ? population.enclos.numero : 'aucun enclos'}" /></p>
				<a href="<c:url value='/population-ajout/${population.id }'/>">Modifier</a><span> - </span><a href="<c:url value='/population-supprimer/${population.id}'/>">Supprimer</a>
			</li>
		</c:forEach>
	</ul>
	<a href="<c:url value='/population-ajout/0'/>">Ajouter une population</a>

<jsp:include page="footer.jsp"></jsp:include>
