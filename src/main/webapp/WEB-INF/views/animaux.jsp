<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>


	<c:if test="${not empty messageErreur }">
		<p><c:out value="${messageErreur }"/></p>
	</c:if>
	<ul>
		<c:forEach items="${animaux}" var="animal">
			<li style="margin-bottom: 10px;">
				<a href="<c:url value='/animal/${animal.id}'/>"><c:out value="${animal.nom}"/></a>
				<br>
				<a href="<c:url value='/animal-ajout/${animal.id }'/>">Modifier</a><span> - </span><a href="<c:url value='/animal-supprimer/${animal.id}'/>">Supprimer</a>
			</li>
		</c:forEach>
	</ul>
	<a href="<c:url value='/animal-ajout/0'/>">Ajouter un animal</a>


<jsp:include page="footer.jsp"></jsp:include>