<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>
	<h3>Ajouter un nouvel animal</h3>
	<form:form servletRelativeAction="/animal-ajout" method="post" modelAttribute="animalDto">
		<form:hidden path="id" />
		<div style="margin-bottom: 20px;">
			<form:label path="nom">Nom de l'animal</form:label>
			<form:input path="nom"/>
			<form:errors path="nom"/>
		</div>
		<div style="margin-bottom: 20px;">
			<form:label path="origine">Origine de l'animal</form:label>
			<form:input path="origine"/>
			<form:errors path="origine"/>
		</div>
		<div style="margin-bottom: 20px;">
			<form:label path="description">Descritpion de l'animal</form:label>
			<form:textarea path="description"/>
			<form:errors path="description"/>
		</div>
		<div style="margin-bottom: 20px;">
			<form:label path="regime">Régime alimentaire de l'animal</form:label>
			<form:select path="regime">
				<form:options items="${regimes}"/>
			</form:select>
		</div>
		<div style="margin-bottom: 20px;">
			<form:label path="nom">Espèce de l'animal ?</form:label>
			<form:select path="especeId">
				<form:options items="${especes}" itemLabel="nom" itemValue="id"/>
			</form:select>
		</div>
		<button type="submit"><c:out value="${(animalDto.id == 0) ? 'Ajouter' : 'Modifier' }" /></button>
	</form:form>


<jsp:include page="footer.jsp"></jsp:include>