<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>

<h1>Liste des enclos</h1>

	<ul>
		<c:forEach items="${enclos}" var="enclo">
		<c:set var="count" value="0" scope="page" />
			<li style="margin: 40px 0;">
			<h2 style="margin: 15px 0;">Numéro de l'enclos : <c:out value="${enclo.numero}" /></h2>
				<h3>Populations à l'intérieur :</h3>
					<ul>
						<c:forEach items="${enclo.populations }" var="population">
							<c:set var="count" value="${count + population.nombreIndividus}" scope="page"/>
							<li>
								<span> Animaux : </span><c:out value="${population.animal.nom}" /><br>
								<span> Taille : </span><c:out value="${population.nombreIndividus}" />
							</li>
						</c:forEach>
					</ul>
					<h4>Nombre total d'individus : <c:out value="${count }" /></h4>
					<a href="<c:url value='/population-ajout/${population.id }'/>">Modifier</a><span> - </span><a href="<c:url value='/population-supprimer/${population.id}'/>">Supprimer</a>
			</li>
		</c:forEach>
	</ul>

<jsp:include page="footer.jsp"></jsp:include>