<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html>
<html lang="fr">
<head>
<meta charset="UTF-8">
<title>Animoz</title>
</head>
<body>
	<nav>
		<a href="<c:url value='/'/>">Accueil</a>
		<a href="<c:url value='/espece'/>">Liste des espèces</a>
		<a href="<c:url value='/animal'/>">Liste des animaux</a>
		<a href="<c:url value='/populations' />" >Liste des Populations</a>
		<a href="<c:url value='/enclos' />" >Liste des enclos</a>
	</nav>