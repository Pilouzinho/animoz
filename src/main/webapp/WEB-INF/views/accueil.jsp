<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>

<h1>Bienvenue sur Animoz</h1>

<a href="<c:url value='/rapport-animaux.pdf' />" target="_blank">Rapport de liste d'animaux PDF</a>
<a href="<c:url value='/rapport.xlsx' />" target="_blank">Tableau liste des populations</a>


<jsp:include page="footer.jsp"></jsp:include>