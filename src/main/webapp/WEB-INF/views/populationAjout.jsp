<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<jsp:include page="header.jsp"></jsp:include>
	
	<h3>Ajouter une nouvelle population</h3>
	<form:form servletRelativeAction="/population-ajout" method="post" modelAttribute="populationDto">
		<form:hidden path="id" />
		<div style="margin-bottom: 20px;">
			<form:label path="idAnimal">Animal qui compose la population</form:label>
			<form:select path="idAnimal">
				<form:option value="0" label="Choisir un animal" />
				<form:options items="${animaux}" itemLabel="nom" itemValue="id"/>
			</form:select>
		</div>
		
		<div style="margin-bottom: 20px;">
			<form:label path="nombreIndividus">Nombre d'individus</form:label>
			<form:input type="number" min="1" path="nombreIndividus"/>
			<form:errors path="nombreIndividus"/>
		</div>
		
		<div style="margin-bottom: 20px;">
			<form:label path="idEnclos">Enclos de la population</form:label>
			<form:select path="idEnclos">
				<form:option value="0" label="Choisir un enclos" />
				<form:options items="${encloss}" itemLabel="numero" itemValue="id"/>
			</form:select>
		</div>
		
		<button type="submit"><c:out value="${(populationDto.id == 0) ? 'Ajouter' : 'Modifier' }" /></button>
	</form:form>

<jsp:include page="footer.jsp"></jsp:include>